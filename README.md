# WayDroid UI

## Dependencies

You'll need to install the following packages on Ubuntu:

* npm
* nodejs
* sqlite3
* lzip

## Running

Run the following command after cloning this repo and [installing WayDroid](https://docs.waydro.id): `npm install && npm start`
