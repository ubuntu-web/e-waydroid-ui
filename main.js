const { app, BrowserWindow, Menu } = require('electron')
const path = require('path')

function createWindow () {
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    icon: 'build/icons/512x512.png',
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  })

  Menu.setApplicationMenu(null)

  mainWindow.loadFile('index.html')
}

app.whenReady().then(() => {
  createWindow()
})
